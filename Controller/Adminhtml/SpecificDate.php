<?php

namespace Webjump\SpecificDate\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Registry;

abstract class SpecificDate extends Action
{

    protected $_coreRegistry;
    const ADMIN_RESOURCE = 'Webjump_SpecificDate::top_level';

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry
    ) {
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Init page
     *
     * @param Page $resultPage
     * @return Page
     */
    public function initPage($resultPage)
    {
        $resultPage->setActiveMenu(self::ADMIN_RESOURCE)
            ->addBreadcrumb(__('Webjump'), __('Webjump'))
            ->addBreadcrumb(__('Webjump Specific Date'), __('Webjump Specific Date'));
        return $resultPage;
    }
}
