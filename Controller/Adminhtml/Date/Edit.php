<?php

declare(strict_types=1);

namespace Webjump\SpecificDate\Controller\Adminhtml\Date;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Webjump\SpecificDate\Api\Data\SpecificDateInterface;
use Webjump\SpecificDate\Api\SpecificDateRepositoryInterface;
use Magento\Framework\App\Action\HttpGetActionInterface;


class Edit extends Action implements HttpGetActionInterface
{
    /**
     * @see _isAllowed()
     */
    const ENTITY_ID = 'Webjump_SpecificDate::entity_id';

    /**
     * @var SpecificDateRepositoryInterface
     */
    private $entityIdRepository;

    /**
     * @param Context $context
     * @param SpecificDateRepositoryInterface $entityIdRepository
     */
    public function __construct(
        Context $context,
        SpecificDateRepositoryInterface $entityIdRepository
    ) {
        parent::__construct($context);
        $this->entityIdRepository = $entityIdRepository;
    }

    /**
     * @inheritdoc
     */
    public function execute(): ResultInterface
    {
        $id = $this->getRequest()->getParam(SpecificDateInterface::ENTITY_ID);
        try {
            $source = $this->entityIdRepository->get($id);
            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('Webjump_SpecificDate::entity_id')
                ->addBreadcrumb(__('Edit Specific Date'), __('Edit Specific Date'));
            $result->getConfig()
                ->getTitle()
                ->prepend(__('Edit Specific Date: %start_date', ['start_date' => $source->getStartDate()]));
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __('Source with specific date "%value" does not exist.', ['value' => $id])
            );
            $result->setPath('*/*');
        }

        return $result;
    }
}
