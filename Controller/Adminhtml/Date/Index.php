<?php
namespace Webjump\SpecificDate\Controller\Adminhtml\Date;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\InventoryApi\Api\SourceRepositoryInterface;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var SourceRepositoryInterface
     */
    protected $sourceRepository;

    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        RequestInterface $request,
        SourceRepositoryInterface $sourceRepository,
        ProductRepositoryInterface $productRepository
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->request = $request;
        $this->sourceRepository = $sourceRepository;
        $this->productRepository = $productRepository;
    }

    /**
     * @return ResultInterface|void
     * @return resultPage
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__("Specific Date"));
        return $resultPage;
    }
}
