<?php
namespace Webjump\SpecificDate\Controller\Adminhtml\Date;

use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Webjump\SpecificDate\Model\SpecificDate;

class Save extends \Magento\Backend\App\Action
{
    protected $dataPersistor;

    /**
     * @param Context $context
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return ResultInterface
     * @throws \Exception
     */
    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data['end_date']) {

            $timezone = new \DateTimeZone('UTC');

            $begin = \DateTime::createFromFormat('d/m/Y', $data['start_date'], $timezone)->format('Y-m-d');
            $end = \DateTime::createFromFormat('d/m/Y', $data['end_date'], $timezone)->format('Y-m-d');

            if ($begin > $end) {
                $this->messageManager->addErrorMessage(__('End date cannot be less than start date.'));
                return $resultRedirect->setPath('*/*/add');
            }
        }

        if ($data) {
            $id = $this->getRequest()->getParam('entity_id');

            /** @var SpecificDate $model */
            $model = $this->_objectManager->create(SpecificDate::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Specific Date no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }

            try {
                $model->saveCustomDate($data);
                $this->messageManager->addSuccessMessage(__('Specific Date saved.'));
                $this->dataPersistor->clear('specific_date');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['entity_id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Specific Date.'));
            }

            $this->dataPersistor->set('specific_date', $data);
            return $resultRedirect->setPath('*/*/edit', ['entity_id' => $this->getRequest()->getParam('entity_id')]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
