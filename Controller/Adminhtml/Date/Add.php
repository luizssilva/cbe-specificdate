<?php

namespace Webjump\SpecificDate\Controller\Adminhtml\Date;

use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Webjump\SpecificDate\Controller\Adminhtml\SpecificDate;

class Add extends SpecificDate
{
    /**
     * @inheritdoc
     */
    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Specific Date'));
        return $resultPage;
    }
}
