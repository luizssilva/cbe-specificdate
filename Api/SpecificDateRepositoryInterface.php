<?php

declare(strict_types=1);

namespace Webjump\SpecificDate\Api;

use Magento\Framework\Api\SearchResultsInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Webjump\SpecificDate\Api\Data\SpecificDateInterface;

interface SpecificDateRepositoryInterface
{

    /**
     * @param int $id
     * @return SpecificDateInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): SpecificDateInterface;

    /**
     * @param SpecificDateInterface $specific
     * @return int
     */
    public function save(SpecificDateInterface $specific): int;

    /**
     * Delete Retries by ID
     * @param int $entityId
     * @return bool true on success
     */
    public function deleteById(int $entityId): bool;

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Webjump\SpecificDate\Api\Data\SpecificDateSearchResultsInterface
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null
    ): \Webjump\SpecificDate\Api\Data\SpecificDateSearchResultsInterface;
}
