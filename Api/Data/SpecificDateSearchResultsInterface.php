<?php

declare(strict_types=1);

namespace Webjump\SpecificDate\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface SpecificDateSearchResultsInterface extends SearchResultsInterface
{
    /**
     *
     * @return SpecificDateInterface[]
     */
    public function getItems();

    /**
     *
     * @param SpecificDateInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}