<?php

declare(strict_types=1);

namespace Webjump\SpecificDate\Api\Data;

interface SpecificDateInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{
    const ENTITY_ID = 'entity_id';
    const START_DATE = 'start_date';
    const END_DATE = 'end_date';
    const DESCRIPTION = 'description';

    /**
     *
     * @return string
     *
     */
    public function getStartDate():string;

    /**
     *
     * @param string $startDate
     * @return void
     */
    public function setStartDate(string $startDate): void;

    /**
     * @return string
     */
    public function getEndDate():string;

    /**
     * @param string $endDate
     * @return void
     */
    public function setEndDate(string $endDate): void;

    /**
     * @return string
     */
    public function getDescription():string;

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void;

}
