<?php

namespace Webjump\SpecificDate\Model\Config\Source;

use Webjump\SpecificDate\Model\SpecificDate;

class Application implements \Magento\Framework\Option\ArrayInterface
{
    protected $_options;
    /**
     * @var SpecificDate
     */
    private $_modelApplication;

    public function __construct(
        SpecificDate $modelApplication
    ) {
        $this->_modelApplication = $modelApplication;
    }

    public function toOptionArray()
    {
        $options  = [];
        foreach ($this->getAttributes() as $key => $value) {
            $options[] = [
                'value' => $key,
                'label' => $value
            ];
        }

        return $options;
    }

    public function toArray()
    {
        $this->_options = $this->getAttributes();
        return  $this->_options;
    }

    public function getAttributes()
    {
        $collection = $this->_modelApplication->getAllLabelApplication();
        $options = [];
        foreach ($collection as $key => $value) {
            $options[$key] = __($value);
        }
        return $options;
    }
}
