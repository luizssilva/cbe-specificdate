<?php
declare(strict_types=1);

namespace Webjump\SpecificDate\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Webjump\SpecificDate\Api\Data\SpecificDateInterface;
use Webjump\SpecificDate\Api\Data\SpecificDateInterfaceFactory;
use Webjump\SpecificDate\Api\Data\SpecificDateSearchResultsInterface;
use Webjump\SpecificDate\Api\Data\SpecificDateSearchResultsInterfaceFactory;
use Webjump\SpecificDate\Api\SpecificDateRepositoryInterface;
use Webjump\SpecificDate\Model\ResourceModel\SpecificDate as SpecificDateResource;
use Webjump\SpecificDate\Model\ResourceModel\SpecificDate\Collection;
use Webjump\SpecificDate\Model\ResourceModel\SpecificDate\CollectionFactory as specificDateCollectionFactory;

class SpecificDateRepository implements SpecificDateRepositoryInterface
{
    protected $specificDateFactory;

    protected $extensibleDataObjectConverter;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    protected $specificDateCollectionFactory;

    protected $dataObjectHelper;

    protected $dataSpecificDateFactory;

    /**
     * @var SpecificDateResource
     */
    private $specificDateResource;

    private $collectionProcessor;

    protected $resource;
    private $specificDateResultsFactory;

    /**
     * @param SpecificDateResource $specificDateResource
     * @param SpecificDateFactory $specificDateFactory
     * @param SpecificDateInterfaceFactory $dataSpecificDateFactory
     * @param SpecificDateCollectionFactory $specificDateCollectionFactory
     * @param SpecificDateSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        SpecificDateResource $specificDateResource,
        SpecificDateFactory $specificDateFactory,
        SpecificDateInterfaceFactory $dataSpecificDateFactory,
        SpecificDateCollectionFactory $specificDateCollectionFactory,
        SpecificDateSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->specificDateResource = $specificDateResource;
        $this->specificDateFactory = $specificDateFactory;
        $this->specificDateCollectionFactory = $specificDateCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataSpecificDateFactory = $dataSpecificDateFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     * @throws CouldNotSaveException
     */
    public function save(
        SpecificDateInterface $specificDate
    ): int {
        $specificDateData = $this->extensibleDataObjectConverter->toNestedArray(
            $specificDate,
            [],
            SpecificDateInterface::class
        );

        $specificDate = $this->specificDateFactory->create()->setData($specificDateData);

        try {
            $this->resource->save($specificDate);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the specificDate: %1',
                $exception->getMessage()
            ));
        }
        return (int)$specificDate->getEntityId();
    }

    /**
     * {@inheritdoc}
     * @throws NoSuchEntityException
     */
    public function getById($specificDateId)
    {
        $specificDate = $this->specificDateFactory->create();
        $this->resource->load($specificDate, $specificDateId);
        if (!$specificDate->getId()) {
            throw new NoSuchEntityException(__('Specific Date with id "%1" does not exist.', $specificDate));
        }
        return $specificDate->getDataModel();
    }

    /**
     * {@inheritdoc}
     * @throws CouldNotDeleteException
     */
    public function delete(
        SpecificDateInterface $specificDate
    ) {
        try {
            $specificDateModel = $this->specificDateFactory->create();
            $this->resource->load($specificDateModel, $specificDate->getspecificDateId());
            $this->resource->delete($specificDateModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Specific Date: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     * @throws NoSuchEntityException
     * @throws CouldNotDeleteException
     */
    public function deleteById($specificDateId) : bool
    {
        return $this->delete($this->getById($specificDateId));
    }

    /**
     * @inheritDoc
     */
    public function get($id): SpecificDateInterface
    {
        /** @var SpecificDateInterface $specificDateInterface */
        $specificDate = $this->specificDateFactory->create();
        $this->specificDateResource->load($specificDate, $id);

        if (!$specificDate->getId()) {
            throw new NoSuchEntityException(
                __('The specific date with the "%1" ID doesn\'t exist. Verify the ID and try again.', $id)
            );
        }

        return $specificDate;
    }

    /**
     * @inheritDoc
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria = null): \Webjump\SpecificDate\Api\Data\SpecificDateSearchResultsInterface
    {
        /** @var Collection $collection */
        $collection = $this->specificDateCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var SpecificDateSearchResultsInterface $searchResult */
        $searchResult = $this->specificDateResultsFactory->create();
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        $searchResult->setSearchCriteria($searchCriteria);
        return $searchResult;
    }
}
