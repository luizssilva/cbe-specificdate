<?php

namespace Webjump\SpecificDate\Model;

use Magento\Framework\Model\AbstractModel;
use Webjump\SpecificDate\Api\Data\SpecificDateInterface;

class SpecificDate extends AbstractModel implements SpecificDateInterface
{
    public $label;

    const ACT_APPLICATION_ALL           = 'all';
    const ACT_APPLICATION_DELIVERY      = 'calendar_delivery';
    const ACT_APPLICATION_DEVOLUTION    = 'calendar_devolution';

    const ORDER_APPLICATION_ALL           = "1";
    const ORDER_APPLICATION_DELIVERY      = "2";
    const ORDER_APPLICATION_DEVOLUTION    = "3";

    /**
     * @var array
     */
    public static $actionOrder = [
        self::ACT_APPLICATION_ALL           => self::ORDER_APPLICATION_ALL,
        self::ACT_APPLICATION_DELIVERY      => self::ORDER_APPLICATION_DELIVERY,
        self::ACT_APPLICATION_DEVOLUTION    => self::ORDER_APPLICATION_DEVOLUTION,
    ];

    protected function _construct()
    {
        $this->_init(\Webjump\SpecificDate\Model\ResourceModel\SpecificDate::class);
    }

    /**
     * @return string
     */
    public function getStartDate(): string
    {
        return $this->getData(self::START_DATE);
    }

    /**
     * @param string $startDate
     */
    public function setStartDate(string $startDate): void
    {
        $this->setData(self::START_DATE, $startDate);
    }

    /**
     * @return string
     */
    public function getEndDate(): string
    {
        return $this->getData(self::END_DATE);
    }

    /**
     * @param string $endDate
     */
    public function setEndDate(string $endDate): void
    {
        $this->setData(self::END_DATE, $endDate);
    }

    public function getDescription(): string
    {
        return  $this->getData(self::DESCRIPTION);
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @return string
     */
    public function getApply(): string
    {
        return $this->getData(self::APPLY);
    }

    /**
     * @param string $apply
     */
    public function setApply(string $apply): void
    {
        $this->setData(self::APPLY, $apply);
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return (bool)$this->getData(self::ACTIVE);
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active): void
    {
        $this->setData(self::ACTIVE, $active);
    }

    /**
     * @return array
     */
    public function getAllLabelApplication()
    {
        if (is_null($this->label)) {
            $this->label = [
                self::ORDER_APPLICATION_ALL => 'Both Calendars',
                self::ORDER_APPLICATION_DELIVERY => 'Delivery Calendar',
                self::ORDER_APPLICATION_DEVOLUTION => 'Devolution Calendar'
            ];
        }
        return $this->label;
    }
    public function getActionApplication($action)
    {
        return (int)self::$actionOrder[$action];
    }

    public function saveCustomDate(array $data)
    {
        $timezone = new \DateTimeZone('UTC');

        if ($data['end_date']) {
            $begin = \DateTime::createFromFormat('d/m/Y', $data['start_date'], $timezone);
            $end = \DateTime::createFromFormat('d/m/Y', $data['end_date'], $timezone);

            $currentDate = $begin;
            while ($currentDate <= $end) {
                $sDate = [
                    'start_date' => $currentDate->format('Y-m-d'),
                    'description' => $data['description'],
                    'apply' => $data['apply'],
                    'active'=> $data['active'],
                ];

                $this->setData($sDate);
                $this->save();

                $currentDate = $currentDate->add(new \DateInterval('P1D'));
            }
        } else {
            $data['start_date'] = \DateTime::createFromFormat('d/m/Y', $data['start_date'], $timezone)->format('Y-m-d');

            $this->setData($data);
            $this->save();
        }
    }
}
