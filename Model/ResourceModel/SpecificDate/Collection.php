<?php

namespace Webjump\SpecificDate\Model\ResourceModel\SpecificDate;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * Event prefix
     *
     * @var string
     */
    protected $_eventPrefix = 'webjump_specificdate_collection';

    /**
     * Event object name
     *
     * @var string
     */
    protected $_eventObject = 'specificdate_collection';

    /**
     * Init collection and determine table names
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Webjump\SpecificDate\Model\SpecificDate::class, \Webjump\SpecificDate\Model\ResourceModel\SpecificDate::class);
    }
}
